# <center>The themes list</center>
These are theme collection from other repo and many more.

## How to use
`clone` theme from the main repository, and move into __theme__ directory in your __hugo__ main folder.

## Table of Content

1. [How to use](#how-to-use)
1. [ToC](#table-of-content)
1. [Themes](#themes)
	2. [Material docs](#hugo-material-docs)
	2. [Theme learn](#theme-learn)
	3. [Arch](#archlinux)
	2. [Universal theme](#universal-theme)
	2. [Hugrid](#hugrid)
	2. [Code Editor Theme](#hugo-code-editor-theme)
	2. [Herring Cove](#herring-cove)
	3. [Identity theme](#identity-theme)
1.	[Disclaimer](#disclaimer)

## Themes
### Hugo Material Docs
![Material docs](hugo-material-docs/images/tn.png)

open terminal and hit `git clone https://github.com/digitalcraftsman/hugo-material-docs.git`

### Hugo Theme Learn
![Theme learn](hugo-theme-learn/images/tn.png)

open terminal and hit `git clone https://github.com/matcornic/hugo-theme-learn.git`

### Archlinux
![Archlinux](hugo-theme-arch/images/tn.png)

open terminal and hit `git clone https://github.com/syui/hugo-theme-arch.git`

### Universal Theme
![Universal theme](hugo-universal-theme/images/tn.png)

open terminal and hit `git clone https://github.com/devcows/hugo-universal-theme.git`

### Hugrid
![Hugrid](hugrid/images/tn.png)

open terminal and hit `git clone https://github.com/aerohub/hugrid.git`

### Hugo Code Editor Theme
![Code editor theme](hugo-code-editor-theme/images/tn.png)

open terminal and hit `git clone https://github.com/aubm/hugo-code-editor-theme.git`

### Herring Cove
![Herring Cove](herring-cove/images/tn.png)

open terminal and hit `git clone https://github.com/spf13/herring-cove.git`

### Identity Theme
![Identity theme](hugo-identity-theme/images/screenshot.png)

open terminal and hit `git clone https://github.com/aerohub/hugo-identity-theme.git`

## Disclaimer
```
I keep it on my repository to make an easiest way to access the document, and i wanna say to all the creator who made this all greate themes.
I keep all like it is, if you wanna make some contribution to the creator just go for it, Thanks.
```